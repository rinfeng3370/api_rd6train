<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InsertApiData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:InsertApiData
                            {--start= : Query startDateTime ex:2019-11-28 11:00:00}
                            {--end= : Query endDateTime ex:2019-11-29 11:00:00}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '匯入時間區段內每分鐘的資料';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datetime = $this->checkInsertDateTime();

        while ($datetime['start'] < $datetime['end']) {
            $job = (new \App\Jobs\DataLoadJob($datetime['start']));
            dispatch($job);

            $datetime['start']->add(new \DateInterval('PT1M'));
        }
    }

    protected function checkInsertDateTime()
    {
        $start_datetime = $this->option('start') ?? $this->defaultStartTime();

        $datetime['start'] = $this->checkDateTime($start_datetime);

        $end_datetime = $this->option('end') ?? $this->defaultEndTime(clone $datetime['start']);

        $datetime['end'] = $this->checkDateTime($end_datetime);

        if ($datetime['start'] >= $datetime['end']) {
            throw new \Exception('時間區間不正確!');
        }

        return $datetime;
    }

    protected function checkDateTime($datetime)
    {
        if (is_string($datetime)) {
            $datetime = \DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
            $errors = \DateTime::getLastErrors();
            if (count($errors['warnings']) || count($errors['errors'])) {
                throw new \Exception('請輸入正確日期!');
            }
        }

        if ($datetime > new \DateTime('now')) {
            throw new \Exception('輸入的日期無效!');
        }

        return $datetime;
    }

    // 沒指定時間，預設為 5 分鐘前
    protected function defaultStartTime()
    {
        $start_time = new \DateTime('-5 min');

        return $start_time->setTime($start_time->format('H'), $start_time->format('i'), 0);
    }

    // 沒指定時間，預設為 starttime的4分59秒後
    protected function defaultEndTime($start_time)
    {
        $start_time->add(new \DateInterval('PT4M'));

        return $start_time->setTime($start_time->format('H'), $start_time->format('i'), 59);
    }
}
