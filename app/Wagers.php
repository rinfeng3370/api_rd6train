<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wagers extends Model
{
    //
    protected $table = "wagers";

    protected $fillable = ['WagersID', 'ServerName', 'Route', 'RoutePath', 'User', 'BetTime'];

    protected $hidden = ['id', 'updated_at'];

    public $timestamps = false;
}
