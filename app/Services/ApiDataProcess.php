<?php
namespace App\Services;

use Illuminate\Support\Facades\Log;
use \Curl\Curl;

class ApiDataProcess
{
    protected $betlog_model;

    public function getApiData($start_datetime, $num)
    {
        $end_datetime = clone $start_datetime;
        $end_datetime->add(new \DateInterval('PT1M'));

        $api_parm = [
            'start' => $start_datetime->format("Y-m-d\TH:i:s"),
            'end' => $end_datetime->format("Y-m-d\TH:i:s"),
            'from' => $num,
        ];

        $curl_params = urldecode(http_build_query($api_parm));

        $curl = new Curl();

        $url = "http://train.rd6/?{$curl_params}";

        echo "$url \n";

        $curl->get($url);

        $curl->close();

        if ($curl->error) {
            Log::channel('queue')->error("Curl Error: $curl->errorCode : $curl->errorMessage");
            throw new \Exception("$curl->errorMessage");
        }

        return json_decode($curl->response, true);
    }

    public function checkDataExist($data)
    {
        $array_key = array_keys($data);
        if ($array_key[0] == "error" || !count($data['hits']['hits']) || empty($data)) {
            return false;
        }
        return true;
    }

    public function processData($data)
    {
        foreach ($data['hits']['hits'] as $i => $value) {
            $bet_time = substr($data['hits']['hits'][$i]['_source']['@timestamp'], 0, 19);
            $bet_time = new \DateTime($bet_time);
            $bet_time->setTimezone(new \DateTimeZone('-4'));
            // 轉換為美東時間
            $data['hits']['hits'][$i]['bet_time'] = $bet_time->format('Y-m-d H:i:s');
            $data['hits']['hits'][$i]['_source'] = stripcslashes(json_encode($value['_source']));
            $data['hits']['hits'][$i]['sort'] = stripcslashes(json_encode($value['sort']));
        }

        return $data;
    }

    public function getModel($name)
    {
        $model_name = "\\App\\$name";

        if (is_null($this->betlog_model) && class_exists($model_name)) {
            $this->betlog_model = new $model_name;
        }

        return $this->betlog_model;
    }

    public function processWagers($betlogs)
    {
        $betlogs = array_values($betlogs);

        $wagers = [];
        
        foreach ($betlogs as $key => $betlog) {
            $betlog['_source'] = json_decode($betlog['_source'],true);
            
            $wagers[$key] = [
                'WagersID' => $betlog['_id'],
                'ServerName' => $betlog['_source']['server_name'],
                'Route' => $betlog['_source']['route'],
                'RoutePath' => $betlog['_source']['route_path'],
                'User' => $betlog['_source']['user'],
                'BetTime' => $betlog['bet_time'],
            ];
        }

        return $wagers;
    }
}
