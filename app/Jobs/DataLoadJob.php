<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DataLoadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $start_datetime;

    protected $num;

    protected $api_data_process;

    protected $queue_repository;

    public function __construct($start_datetime, $num = 0)
    {
        $this->start_datetime = $start_datetime;
        $this->num = $num;
        $this->api_data_process = new \App\Services\ApiDataProcess;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->api_data_process->getApiData($this->start_datetime, $this->num);

        $data_exist = $this->api_data_process->checkDataExist($data);

        if (!$data_exist) {
            return;
        }

        $data_size = 10000;

        $processed_data = $this->api_data_process->processData($data);

        foreach (array_chunk($processed_data['hits']['hits'], 1000) as $pocess_betlog) {
            $job = (new WriteBetlog($pocess_betlog));
            dispatch($job);
        }

        $this->num += $data_size;

        if (count($data['hits']['hits']) === $data_size) {
            $job = (new \App\Jobs\DataLoadJob($this->start_datetime, $this->num));
            dispatch($job);
        }
    }
}
