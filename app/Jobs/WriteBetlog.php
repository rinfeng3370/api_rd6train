<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WriteBetlog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $betlogs;

    protected $api_data_process;

    public function __construct($betlogs)
    {
        $this->betlogs = $betlogs;
        $this->api_data_process = new \App\Services\ApiDataProcess;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $process_betlogs = [];

        $betlog_model = $this->api_data_process->getModel("Queue");

        $queue_repository = new \App\Repositorys\QueueRepository($betlog_model);

        $process_betlogs = $queue_repository->InsertOrUpdateData($this->betlogs, '_id');

        if (empty($process_betlogs)) {
            return;
        }

        $job = (new \App\Jobs\Wagers($process_betlogs));
        dispatch($job);
    }
}
