<?php
namespace App\Repositorys;

use Illuminate\Support\Facades\Log;

class QueueRepository
{
    protected $model;

    protected $api_data_process;

    public function __construct(\Illuminate\DataBase\Eloquent\Model $model)
    {
        $this->model = $model;
        $this->api_data_process = new \App\Services\ApiDataProcess;
    }

    public function dbRepeatSearch($unique_key, $pluck_key)
    {
        return $this->model->wherein($unique_key, $pluck_key)->get()->keyBy($unique_key)->toArray();
    }

    public function InsertOrUpdateData($data, $unique_key)
    {
        $processed_data = [];

        $data = $this->getUpdateOrInsertData($data, $unique_key);

        $this->model->insert($data['insert']);

        $processed_data = $data['insert'];
        
        foreach ($data['update'] as $row) {
            if ($this->model->where('_id', $row['_id'])->update($row)) {
                Log::channel('queue')->info("_id:{$row['_id']}，此筆資料更新成功");
                $processed_data[] = $row;
            } else {
                Log::channel('queue')->notice("_id:{$row['_id']}，此筆資料更新失敗 ");
            }
        }
        
        return $processed_data;
    }

    protected function getUpdateOrInsertData($temp, $unique_key)
    {
        $data_collection = collect($temp);

        $pluck_key = $data_collection->pluck($unique_key)->all();

        $db_repeat_list = $this->model->wherein($unique_key, $pluck_key)->get()->keyBy($unique_key)->toArray();

        $repeat_key = array_column($db_repeat_list, $unique_key);

        $repeat_data = $data_collection->whereIn($unique_key, $repeat_key)->keyBy($unique_key)->all();

        $results['insert'] = $data_collection->whereNotIn($unique_key, $repeat_key)->all();

        $results['update'] = [];

        foreach ($repeat_data as $key => $value) {
            $db_repeat = collect($db_repeat_list[$key]);

            $diff = $db_repeat->diffAssoc($value)->count();

            if ($diff) {
                Log::channel('queue')->info("_id:{$value['_id']}，此筆資料有異動需要更新 ");
                $results['update'][] = $value;
            }
        }
        return $results;
    }
}
