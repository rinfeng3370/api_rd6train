<?php

namespace App\Http\Controllers;

use App\Concrete\DataConcrete;
use App\Jobs\InsertJob;
use App\Jobs\UpdateJob;
use \Curl\Curl;

class queueController extends Controller
{
    //
    protected $unis = [];

    protected $unis_key = [];

    protected $controller_array = [];

    protected $data_concrete;

    public function __construct()
    {
        $this->data_concrete = new DataConcrete;
    }

    public function index($url)
    {

        $curl = new Curl();

        echo $url . "\n";

        $curl->get($url);

        if ($curl->error) {
            echo 'Error: ' . $curl->errorCode . ':' . $curl->errorMessage . "\n";
        }

        echo 'Response:' . "\n";

        $data = json_decode($curl->response, true);

        $controller_array['pass'] = $this->checkDataExist($data);  

        if ($controller_array['pass']) {

            foreach ($data['hits']['hits'] as $i => $value) {
                $data['hits']['hits'][$i]['_source'] = stripcslashes(json_encode($value['_source']));
                $data['hits']['hits'][$i]['sort'] = stripcslashes(json_encode($value['sort']));
            }

            $data_num = count($data['hits']['hits']);
            echo "資料量: $data_num \n";

            $controller_array = $this->data_concrete->processRepeatData($data['hits']['hits']);
            $count = 0;
            foreach ($controller_array['data_chunks'] as $item) {
                InsertJob::dispatch($item, $count);
                $count += 1000;
            }

            if (isset($controller_array['unis_chunks'])) {
                foreach ($controller_array['unis_chunks'] as $key => $uni_chunk) {
                    UpdateJob::dispatch($uni_chunk, $controller_array['unis_key_chunks'][$key], $count);
                    $count += 1000;
                }
            }
        }
        $curl->close();
    }

    protected function checkDataExist($data)
    {
        $array_key = array_keys($data);
        if ($array_key[0] == "error" || !count($data['hits']['hits'])) {
            echo '發生錯誤或無資料!' . "\n";
            return false;
        }
        return true;
    }
}
